Metapackage of Corin controller and planner.

Author: Wei Cheah
Maintainer: Wei Cheah, wei.cheah@manchester.ac.uk
With contributions by: Hassan Khalili
Affiliation: Robotics for Extreme Environmente Labaratory, University of Manchester
