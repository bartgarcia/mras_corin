# corin_control

## Overview

This package is the robot's core controller. The file 'main.py', creates an instance of the robot controller and all actions are invoked from here. 

## Demonstrations
To run a prescripted motion:

        rosrun corin_control main.py
        
## Subscribed Topics

The primary topic of interest is the robot joint states. The topic name and type differs depending on the controller it is interfaced to, either Gazebo or Dynamixel joints.

* **`/corin/joint_states`** ([sensor_msgs/JointState])

    The joint states of the robot in Gazebo.

Other useful topics are the IMU, robot state model (for Gazebo only), user interface:

* **`/corin/imu/base/data`** ([sensor_msgs/Imu])

* **`/gazebo/model_states`** ([gazebo_msgs/ModelStates])

## Published Topics

The publisher to control the joints differs depending on the controller it is interfaced to (Gazebo, Dynamixel or RViZ). The RViZ interface enables fast simulation of robot motion without physics engine dependency. 

* **`/corin/joint_states`** ([sensor_msgs/JointState])

    This topic is used for RViZ interface.
