#!/usr/bin/env python

## Sets Gazebo Corin model to default world position in nominal standing up position

import rospy, sys, os, time
import string
import warnings
import tf
from math import pi

from gazebo_msgs.srv import *
from gazebo_ros import gazebo_interface
from std_srvs.srv import Empty
from gazebo_msgs.msg import ModelState
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Twist
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState


def set_model_pose(model_name, ref_frame, p6pose):
	""" Sets the model state """

	model_state = ModelState()
	
	model_state.pose = p6pose
	model_state.model_name = model_name
	model_state.reference_frame = ref_frame

	gazebo_namespace = '/gazebo'

	try:
		rospy.wait_for_service('%s/set_model_state'%(gazebo_namespace), 2)
		set_model_state = rospy.ServiceProxy('%s/set_model_state'%(gazebo_namespace), SetModelState)
		rospy.loginfo("Calling service %s/set_model_state"%gazebo_namespace)
		resp = set_model_state(model_state)
		rospy.loginfo("Set model state status: %s"%resp.status_message)
		print resp.status_message

	except rospy.ServiceException as e:
		print("Service call failed: %s" % e)
	except rospy.ROSException as e:
		# Gazebo inactive
		print("Service call failed: %s" % e)

def deg2rad(q):
	return (q*pi/180)

if __name__ == "__main__":

	rospy.init_node('initialise_node') 		#Initialises node

	model_name = 'corin'
	reference_frame	= 'world'

	# joint publisher
	joint_pub = {}
	joint_pub[0] = rospy.Publisher(model_name + '/lf_q1_joint/command', Float64, queue_size=1)
	joint_pub[1] = rospy.Publisher(model_name + '/lf_q2_joint/command', Float64, queue_size=1)
	joint_pub[2] = rospy.Publisher(model_name + '/lf_q3_joint/command', Float64, queue_size=1)
	joint_pub[3] = rospy.Publisher(model_name + '/lm_q1_joint/command', Float64, queue_size=1)
	joint_pub[4] = rospy.Publisher(model_name + '/lm_q2_joint/command', Float64, queue_size=1)
	joint_pub[5] = rospy.Publisher(model_name + '/lm_q3_joint/command', Float64, queue_size=1)
	joint_pub[6] = rospy.Publisher(model_name + '/lr_q1_joint/command', Float64, queue_size=1)
	joint_pub[7] = rospy.Publisher(model_name + '/lr_q2_joint/command', Float64, queue_size=1)
	joint_pub[8] = rospy.Publisher(model_name + '/lr_q3_joint/command', Float64, queue_size=1)
	joint_pub[9] = rospy.Publisher(model_name + '/rf_q1_joint/command', Float64, queue_size=1)
	joint_pub[10] = rospy.Publisher(model_name + '/rf_q2_joint/command', Float64, queue_size=1)
	joint_pub[11] = rospy.Publisher(model_name + '/rf_q3_joint/command', Float64, queue_size=1)
	joint_pub[12] = rospy.Publisher(model_name + '/rm_q1_joint/command', Float64, queue_size=1)
	joint_pub[13] = rospy.Publisher(model_name + '/rm_q2_joint/command', Float64, queue_size=1)
	joint_pub[14] = rospy.Publisher(model_name + '/rm_q3_joint/command', Float64, queue_size=1)
	joint_pub[15] = rospy.Publisher(model_name + '/rr_q1_joint/command', Float64, queue_size=1)
	joint_pub[16] = rospy.Publisher(model_name + '/rr_q2_joint/command', Float64, queue_size=1)
	joint_pub[17] = rospy.Publisher(model_name + '/rr_q3_joint/command', Float64, queue_size=1)

	rospy.sleep(0.5)
	
	qf = [0.165,	0.334,	-2.00, -0.188,	0.336,	-1.82, 0.129,	0.310,	-1.64, 0.129,	0.310,	-1.64, -0.188,	0.336,	-1.82, 0.165,	0.334,	-2.00]

	for i in range(0,5):
		for j in range(0,18):
			qj = Float64()
			qj.data = qf[j]
			joint_pub[j].publish(qj)

		rospy.sleep(0.2) 	# small delay

	print 'Joint States Set Complete!'

	## Set Corin state
	pose  = Pose()
	pose.position.x		= 0.0
	pose.position.y		= 0.0
	pose.position.z		= 0.15
	roll 	= 0.0
	pitch = 0.0
	yaw 	= 0.0
	quaternion	= tf.transformations.quaternion_from_euler(deg2rad(roll), deg2rad(pitch), deg2rad(yaw))
	pose.orientation.x	= quaternion[0]
	pose.orientation.y	= quaternion[1]
	pose.orientation.z	= quaternion[2]
	pose.orientation.w	= quaternion[3]
	set_model_pose(model_name, reference_frame, pose)

	## Set obstacle state
	model_name = 'obstacle'
	pose  = Pose()
	pose.position.x		= 0.0
	pose.position.y		= 0.0
	pose.position.z		= 0.15
	set_model_pose(model_name, reference_frame, pose)
	
	# set joint states again
	for i in range(0,5):
		for j in range(0,18):
			qj = Float64()
			qj.data = qf[j]
			joint_pub[j].publish(qj)

		rospy.sleep(0.2) 	# small delay