#!/usr/bin/env python

""" Read CSV file and publish joint setpoint to Gazebo robot """

import rospy
from constant import *
from std_msgs.msg import Float64

import csv
import numpy as np

class RvizSnapshot:
	def __init__(self):
		rospy.init_node('CorinController') 		# Initialises node
		self.rate 	  = rospy.Rate(CTR_RATE)	# Controller rate
		self.joint_pub_ = {} 									# Topic list

		self.t = []
		self.qstate = []

		self.__initialise_topics__()
		
	def __initialise_topics__(self):
		## Publish to Gazebo
		for i in range(0,18):
			self.joint_pub_[i] = rospy.Publisher(ROBOT_NS + '/' + JOINT_NAME[i] + '/command', Float64, queue_size=1)

	def publish(self, q):

		# Publish joint angles individually
		for n in range(0,18):
			qp = Float64()
			qp.data = q[n]
			self.joint_pub_[n].publish(qp)
		
	def load_file(self, filename):
		print 'Reading file: ', filename
		
		with open(filename, 'rb') as csvfile:
			motion_data = csv.reader(csvfile, delimiter=',')
			counter = 0
			for row in motion_data:
				# Append for time
				self.t.append(row[0])
				# Append for CoB
				self.qstate.append( np.asarray(map(lambda x: float(x), row[1:19])) )

	def cycle_snapshot(self):

		# for i in range(0,len(self.CoB)):
		i = 0
		while (i != len(self.t) and not rospy.is_shutdown()):
			
			# for xi in range(0,2):
			qp = self.qstate[i].copy()
			self.publish(qp)
			
			i += 1			
			rospy.sleep(0.1)
			# raw_input('continue')

if __name__ == "__main__":

	rviz = RvizSnapshot()
	
	# rviz.load_file('bodypose.csv')
	rviz.load_file('obstacle.csv')

	raw_input('Start motion!')
	for i in range(0,1):
		rviz.cycle_snapshot()