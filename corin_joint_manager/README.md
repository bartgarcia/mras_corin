# corin_manager

## Overview
This node launches the controller manager for EITHER the Dynamixel motors or Gazebo joints used on the Corin hexapod. 
Gazebo: The joints are controlled using position controller.

## Demonstration
To control the robot's joints using position controller for:
### Gazebo

        roslaunch corin_manager corin_manager.launch
