# Branch
Gazebo simulation for Mobile Robotics and Autonomous Systems module.
Corin executing a bodypose and walking up an obstacle. 

# Corin
TL;DR: Code for Corin. Head to 'Demonstrations' section on how to use this code.

## Overview

This is a python library with [ROS] interface to control the Corin hexapod. There are some C++ tools but these are not used.

Features:

* **ROS interface:** The robot and the core modules of the library interfaces to ROS. A number of library functions are able to run independantly.
* **Visualizations:** The robot can be visualised in RViZ and Gazebo.
* **Physics Engine:** The gazebo simulator enables evaluation of motion control algorithms in real-world scenarios.

The Corin package has been tested with [ROS] Melodic (under Ubuntu 18.04). This is research code, expect that it changes often and any fitness for a particular purpose is disclaimed.

The source code is released under a proprietary software license and should not be released to any person without the author's permission.

**Author: Wei Cheah**
**Maintainer: Wei Cheah, wei.cheah@manchester.ac.uk**
**With contributions by: Hassan Hakim Khalili**
**Robotics for Extreme Environment Group, University of Manchester**

## Installation

### Building from Source

#### Dependencies

The dependencies required should have been installed with the standard ROS installation.

#### Building

To build from source, clone the latest version from this repository into your catkin workspace and compile the package using 

    cd catkin_ws/src
    git clone https://wilsonz91@bitbucket.org/wilsonz91/corin.git
    cd ../
    catkin_make

### Packages Overview

This repository consists of following packages:

* ***corin*** is the meta-package for the Corin library.
* ***corin_control*** implements the algorithms of the Corin library. 
* ***corin_description*** contains the urdf description of Corin and the RViZ visualizer launch file.
* ***corin_gazebo*** contains the launch file for the gazebo simulator and the different worlds.
* ***corin_joint_manager*** is the node to interface with the Dynamixel motors on Corin using ROBOTIS-Framework package.

## Usage

### Demonstrations
Three terminals are required to run the full demonstration. Create a terminal and source the ROS bash file for each of them via

        cd ~/catkin_ws
        source devel/setup.bash 

The Gazebo simulator for Corin is launched using:

        roslaunch corin_gazebo corin_world.launch

To control the robot's joints using position controller in Gazebo:

        roslaunch corin_joint_manager corin_joint_manager.launch

To run a prescripted motion, first navigate to the working directory in which the csv files are stored:

        cd ~/catkin_ws/src/corin/corin_control/src
        rosrun corin_control main.py

The code will fail to run if executed from the wrong directory as it will not be able to find the required csv file.

There are two demos provided, one for stepping onto an obstacle, the other for bodypose. These can be selected via line 66-67 in main.py. 

Note: Run in the terminal either one of the following to set the robot to its default position for the respective demos:

        rosrun corin_control obstacle_default.py
        rosrun corin_control bodypose_default.py

## Packages

### corin_joint_manager

This node launches the controller manager for the Corin hexapod.

### corin_gazebo

This node launches the Gazebo simulator with the Corin hexapod. The default settings launch the robot in an empty environment. Different worlds can be created and should be stored in 'worlds'. This will allow easy access during launch by passing the name of the world.

### corin_description

This node launches the RViZ visualizer. The visualizer allows visualisation of robot states, maps, markers, etc. The difference to Gazebo is that there is no physics engine running, and is commonly used to visualise the actual robot states. Currently, it has been setup to be used as a visualizer for evaluating motion planning algorithms rather than viewing the actual robot state. 

### corin_control

This package is the robot's core controller. The launch files in this package is used to control the robot's joints in Gazebo. 